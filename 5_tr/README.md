# Tr

## Problems

Consider a `file1` with following fields: `index`, `date`, `name`, `grade` separated by `&`.

1. Change field delimiter from `&` to `+`.
2. Change all alphabetical letters from lower case to upper case.
3. Remove all symbols in string `123`.
4. Replace repeated spaces with just one space.

## Solutions

1. `cat 5_tr/file1 | tr "&" "+"`
2. `cat 5_tr/file1 | tr "[:lower:]" "[:upper:]"`
3. `cat 5_tr/file1 | tr --delete "123"`
4. `cat 5_tr/file1 | tr --squeeze-repeats " "`
