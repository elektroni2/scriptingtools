# Grep

## Problems

Consider a `file1` with following fields: `index`, `date`, `name`, `grade` separated by `&`.

1. Find lines from `file1` which contain two or more adjacent spaces.
2. Find lines from `file1` which contain month 9.
3. Find lines from `file1` which contain year `19...` and grade over `50`.

## Solutions

1. `cat 9_grep/file1 | grep " \{2,\}"`
2. `cat 9_grep/file1 | grep "[[:digit:]]\{4\}\-09"`
3. `cat 9_grep/file1 | grep "19[[:digit:]]\{2\}\-" | grep ".*&.*&.*&[5-9]\{1\}.*"`
