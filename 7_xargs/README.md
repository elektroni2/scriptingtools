# Xargs

`<standard output> | xargs <command>`

Feeds standard input as space separated arguments to given command.

## Problems

1. Find all files beginning with `file` and print their contents.
2. Find all files beginning with `file` and copy them to `output` folder.
3. Find and remove all files inside folder `output` that begin with `file`.

## Solutions

1. `find 7_xargs/ -name "file*" | xargs cat`
2. `find 7_xargs/ -name "file*" | xargs --replace=µ cp µ ./7_xargs/output`
3. `find 7_xargs/output/ -name "file*" | xargs rm`
