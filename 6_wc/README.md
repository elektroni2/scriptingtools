# Wc

## Problems

Consider files `file1` and `file2` with textual content.

1. Print the amount of words in `file1`.
2. Print the amount of words in both files.

## Solutions

1. `wc --words 6_wc/file1 | cut --delimiter " " --field 1`
2. `wc --words 6_wc/file1 6_wc/file2`
