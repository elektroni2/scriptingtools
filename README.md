# Scriptingtools

This repository contains simple example use cases for common scripting tools.

Each script provides a solution to a problem.
Thus each description follows **problem** and **solution** format.

## Configuration

Configure the environment first.

`chmod +x configure`

`./configure`

## Useful sources

- [bash](https://www.gnu.org/software/bash/manual/bash.html)
- [coreutils](https://www.gnu.org/software/coreutils/manual/coreutils.html)
- [Filesystem Hierarchy Standard](https://refspecs.linuxfoundation.org/FHS_3.0/fhs/index.html)

---

## Bash workflow patterns

### Sequence commands `|`

Feed stdout to next command.

`command1 | command2 | command3`

```mermaid
flowchart LR

command1 --stdout--> command2
command2 --stdout--> command3
command3 --stdout--> display

command1 --stderr--> display
command2 --stderr--> display
command3 --stderr--> display
```

### Sequence commands `|&`

Feed stdout and stderr to next command.

`command1 |& command2 |& command3`

```mermaid
flowchart LR

command1 --stdout--> command2
command2 --stdout--> command3
command3 --stdout--> display

command1 --stderr--> command2
command2 --stderr--> command3
command3 --stderr--> display
```

### Sequence pipelines `;`

Execute next pipeline no matter what.

`pipeline1 ; pipeline2 ; pipeline3`

```mermaid
flowchart LR

pipeline1 --> pipeline2
pipeline2 --> pipeline3
pipeline3 --> finish
```

### Exclusive choice `&&`

Execute next pipeline if current succeeds.

`pipeline1 && pipeline2 && pipeline3`

```mermaid
flowchart LR

pipeline1 --0--> pipeline2
pipeline2 --0--> pipeline3
pipeline3 --0--> finish

pipeline1 --> finish
pipeline2 --> finish
pipeline3 --> finish
```

`true && echo "hello!"`

`false && echo "no hello"`

### Exclusive choice `||`

Execute next pipeline if current fails.

`pipeline1 || pipeline2 || pipeline3`

```mermaid
flowchart LR

pipeline1 --!0--> pipeline2
pipeline2 --!0--> pipeline3
pipeline3 --!0--> finish

pipeline1 --> finish
pipeline2 --> finish
pipeline3 --> finish
```

`true && echo "no hello"`

`false && echo "hello!"`

### Parallel split `pipeline&`

Execute pipelines in parallel.

`pipeline1 & pipeline2 & pipeline3 &`

```mermaid
flowchart LR

start --> pipeline1 --> finish
start --> pipeline2 --> finish
start --> pipeline3 --> finish
```

`echo "bla1" && echo "bla1" && echo "bla1" & echo "bla2" && echo "bla2" && echo "bla2" && echo "bla2" &`

### Shell control flow

These structures exist.

`until ...; do ...; done`

`while ...; do ...; done`

`for ...; do ...; done`

`if ...; then ...; elif ...; then ...; else ...; fi`

`case ... in ...;; ...;; esac`

### Define a function

`function_name () { commands; }`

Function without arguments:

`say_hello () { echo "hello"; }`

Function with arguments:

`say () { echo "$1"; }`

`how_many_arguments () { echo "$#"; }`

### Define a variable

Variable is also known as "shell parameter".

`my_name="Mr. Shell"`

---
Notice!

Following will fail because Bash thinks that `$my_name` evaluates to two parameters.
`say $my_name`

Fix: enclose the variable with double quotes.
`say "$my_name"`

---

### Shell expansions

These concepts exist.
- Brace expansion
- Tilde expansion
- Shell parameter expansion `${...}`
- Command substitution
- Arithmetic expansion
- Process substitution
- Word splitting
- Filename expansion
- Quote removal

### Some hotkey tips

- `CTRL + L` executes `clear`.
- `CTRL + ARROW` moves forward or backward a single word.
- `TAB` autocomplete.
- `CTRL + R` search a command from history.
