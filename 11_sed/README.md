# Sed

## Problems

Consider a `file1` with following fields: `index`, `date`, `name`, `grade` separated by `&`.

1. Insert header line with field names before first line.
2. Append new line with data `100&2018-03-03T03:00:00&eaa sh&100`.
3. Substitute `2022` with `2021`.

## Solutions

1. `cat 11_sed/file1 | sed "1i\index&date&name&grade"`
2. `cat 11_sed/file1 | sed '$a\100&2018-03-03T03:00:00&eaa sh&100'`
3. `cat 11_sed/file1 | sed 's/2022/2021/g'`
