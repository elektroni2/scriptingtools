# Shebang

## Problem

1. User orders kernel to execute file X.
2. Kernel finds out that X is a text file.
3. Kernel concludes that an interpreter is needed to execute X.
4. **Which interpreter to use?**

## Solution

Use shebang to select which executable is used to process the containing file.

`#!<absolute path to interpreter> [one optional argument]`

## How it works?

1. Kernel reads the first two bytes (#!) of X. [source](https://github.com/torvalds/linux/blob/5bfc75d92efd494db37f5c4c173d3639d4772966/fs/binfmt_script.c#L34)
2. Kernel recognizes that shebang is used and parses the interpreter path and argument.
3. Kernel feeds the containing file as an argument for the interpreter.
4. Script gets executed.

## Example `file1`

`./1_shebang/file1`

This file is supposed to be executed using shell `sh`.
This executable is part of the [essential user command binaries](https://refspecs.linuxfoundation.org/FHS_3.0/fhs/ch03s04.html).
Thus it is found from path `/bin/sh`.
Thus the shebang is constructed as `#!/bin/sh`.

## Example `file2`

`./1_shebang/file2`

This file is supposed to be executed using Python 3 interpreter `python3`.
This executable can be located anywhere in the filesystem.
Usually it is located in `/usr/bin`, so it would make sense to use `/usr/bin/python3`.
But because Python is not part of the essential user commands, this is not the case in all platforms.
Thus it would make sense to search Python's path from user's path environment variable.
Thus the shebang is constructed as `#!/usr/bin/env python3`.

The [`env`](https://man7.org/linux/man-pages/man1/env.1.html) runs an executable in a modified environment.
The executable `python3` is (hopefully!) found from the environment variable `PATH`.

---
**Notice!**
The `env` is not part of essential user commands, so it is not necessarily found on all platforms.
Thus it's location is not standardized, so `/usr/bin/env` is considered *best practice*.

---
