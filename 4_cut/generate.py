#!/usr/bin/env python3

"""
Generate example files.
"""

import random
import string
from pathlib import Path
from datetime import datetime


def random_date() -> str:
    timestamp_max = int(datetime.today().timestamp())
    timestamp = random.randint(1, timestamp_max)
    return datetime.fromtimestamp(timestamp).isoformat(timespec="seconds")


def random_name() -> str:
    length = random.randint(1, 64)
    letters = string.ascii_lowercase + " "
    name = random.choices(letters, k=length)
    return "".join(name)


if __name__ == "__main__":
    rows = 100
    separator = "&"
    indices = [index for index in range(rows)]
    random.shuffle(indices)
    with Path("4_cut/file1").open("w") as stream:
        for i in range(rows):
            date = random_date()
            name = random_name()
            value = str(random.randint(0, rows))
            index = str(indices[i])
            line = separator.join([index, date, name, value])
            stream.write(f"{line}\n")
