# Cut

## File problems

Consider a `file1` with following fields: `index`, `date`, `name`, `grade` separated by `&`.

1. Extract field `index`.
2. Extract years from field `date`.

## Solutions

1. `cut --delimiter "&" --field 1 4_cut/file1`
2. `cut --delimiter "&" --field 2 4_cut/file1 | cut --delimiter "-" --field 1`

---

## Complex problems

1. Extract last path from `PATH` environment variable.

## Solutions

1. `env | grep "^PATH" | cut --delimiter "=" --field 2 | rev | cut --delimiter ":" --field 1 | rev`
