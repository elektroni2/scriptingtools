# Find

## Problems

1. Find files which start `file` and end with `.md`.
2. Find files which start `file` but do not contain `2`.
3. Find files which start `file` but are over 1 kB in size.
4. Find files which start `file` but exclude subfolder `subfolder`.

## Solutions

1. `find 8_find/ -name "file*.md"`
2. `find 8_find/ -name "file*" -and -not -name "*2*"`
3. `find 8_find/ -name "file*" -and -not -size 1k`
4. `find 8_find/ -name "file*" -and -not -path "8_find/subfolder/*"`
