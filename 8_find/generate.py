#!/usr/bin/env python3

"""
Generate example files.
"""

from typing import List
from pathlib import Path


def create_files(base: str, names: List[str], contents: List[str]) -> None:
    for file, content in zip(names, contents):
        with Path(base + file).open("w") as stream:
            stream.write(f"{content}\n")


if __name__ == "__main__":
    create_files(
        base="8_find/",
        names=["file1", "file2.md", "file3.md"],
        contents=[
            "Elämä on laiffii.",
            "He's a highly trained professional. We have assured the Administrator that nothing will go wrong.",
            "Not great, not terrible.",
        ],
    )
    create_files(
        base="8_find/subfolder/",
        names=["file4.txt"],
        contents=["".join(["A" for _ in range(10000)])],
    )
