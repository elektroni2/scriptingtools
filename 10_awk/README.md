# Awk

---
Notice!
AWK uses a program to filter and extract data from a file or standard input.
This program has it's own syntax and [functions](https://www.gnu.org/software/gawk/manual/html_node/Library-Functions.html) and it can be arbitrarily complex.

---

## Problems

Consider a `file1` with following fields: `index`, `date`, `name`, `grade` separated by `&`.

1. Print all dates.
2. Print length of longest name.

## Solutions

1. `cat 10_awk/file1 | awk --field-separator "&" '{print $2}'`
2. `cat 10_awk/file1 | awk --field-separator "&" '{ if (max < (length($0))) max = length($0) } END { print max }'`
