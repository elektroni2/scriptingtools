# Sort

## File problems

Consider a `file1` with following fields: `index`, `date`, `name`, `grade` separated by `&`.

1. Sort the lines of `file1` by index in ascending order.
2. Sort the lines of `file1` by date in ascending order.
3. Sort the lines of `file1` by grade in descending order.

## Solutions

1. `sort --numeric-sort 2_sort/file1`
2. `sort --field-separator "&" --key 2 2_sort/file1`
3. `sort --field-separator "&" --key 4 --reverse 2_sort/file1`

---

## Complex problems

1. Print all process identifiers and associated users, sort by identifier in ascending order.

## Solutions

1. `ps -eo pid,user --no-headers | sort --numeric-sort`
