#!/usr/bin/env python3

"""
Generate example files.
"""

import random
from pathlib import Path

if __name__ == "__main__":
    rows = 100
    lines = [
        "cat",
        "dog",
        "duck",
        "goose",
        "hen",
        "horse",
        "pig",
    ]
    with Path("3_uniq/file1").open("w") as stream:
        for i in range(rows):
            line = random.choice(lines)
            stream.write(f"{line}\n")
