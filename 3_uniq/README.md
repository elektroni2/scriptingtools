# Uniq

---

## File problems

Consider a door in a zoo with animal detector.
Detection results are written to `file1` in order of occurence.

```mermaid
flowchart LR
a1[duck]-->a2[duck]-->a3[goose]-->a4[horse]-->a5[goose]-->a6[...]
```

1. Filter adjacent detections so that each next line has animal other than current line.
2. Get animals who moved in groups.
3. Count how many times the same animal occured successively.
4. Count total occurences of each animal, sort by count in descending order.

## Solutions

1. `uniq 3_uniq/file1`
2. `uniq --repeated 3_uniq/file1`
3. `uniq --count 3_uniq/file1`
4. `sort 3_uniq_file1 | uniq --count | sort --numeric-sort --reverse`

---

## Complex problems

1. Get count of each user's processes sorted by process count in ascending order.

## Solutions

1. `ps -eo user --no-headers | sort | uniq --count | sort --numeric-sort`
